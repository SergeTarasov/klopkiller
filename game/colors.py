import pygame as pg

BLACK = pg.Color(0, 0, 0)
WHITE = pg.Color(255, 255, 255)

RED = pg.Color(255, 0, 0)
GREEN = pg.Color(0, 255, 0)
BLUE = pg.Color(0, 0, 255)
