import os
import pygame as pg
import pygame.freetype as pg_font

from random import randint, choice

from .utils import ListSelection, PositionPropertyMixin, SizePropertyMixin
from .colors import (
    BLACK, WHITE, RED, BLUE
)
from .constants import POISON_IMAGE_SETS, KK_IMAGES

if not pg.image.get_extended():
    SystemExit("Не удалось подгрузить расширенный модуль картинок.")

MAIN_DIR = os.path.split(os.path.abspath(__file__))[0]
DEFAULT_FONT = None
SCREEN_SIZE = 800, 600

# 5 min in frames
C_1MIN = 60 * 60
C_2MIN = 2 * 60 * 60
C_4MIN = 5 * 60 * 60
C_5MIN = 5 * 60 * 60
C_10MIN = 10 * 60 * 60
C_20MIN = 20 * 60 * 60


# ------------------------------------------- HELPERS -------------------------------------------
def load_image(file):
    """
    loads an image
    """
    file = os.path.join(MAIN_DIR, "media", file)
    try:
        surface = pg.image.load(file)
    except pg.error:
        raise SystemExit('Could not load image "%s" %s' % (file, pg.get_error()))
    return surface.convert()


def load_sound(file):
    """
    loads a sound
    """
    if not pg.mixer:
        # pygame can be compiled without mixer
        return None
    file = os.path.join(MAIN_DIR, "media", file)
    try:
        sound = pg.mixer.Sound(file)
        return sound
    except pg.error:
        print("Warning, unable to load, %s" % file)
    return None


class Scene:
    go_next = False
    next_scene = None
    music = None
    BACKGROUND = WHITE

    def __init__(self, screen):
        self.frame_counter = 0
        self.screen = screen
        self.screen_size = screen.get_size()
        self.container = pg.sprite.RenderUpdates()
        self.background = self.get_background_surface()

        if type(self.music) is str:
            self.music = load_sound(self.music)
            self.music.play(-1)

    def get_background_surface(self):
        background = pg.Surface(self.screen_size)

        if type(self.BACKGROUND) is pg.Color:
            background.fill(self.BACKGROUND)
        elif type(self.BACKGROUND) is str:
            bg_file = load_image(self.BACKGROUND)
            background.blit(bg_file, (0, 0))
        else:
            background.blit(self.BACKGROUND, (0, 0))

        self.screen.blit(background, (0, 0))
        pg.display.flip()
        return background

    def clear(self):
        self.container.clear(self.screen, self.background)

    def process_input(self, events, pressed_keys):
        pass

    def update(self):
        self.frame_counter += 1
        self.container.update()

    def render(self):
        pg.display.update(self.container.draw(self.screen))

    def terminate(self):
        if self.music:
            self.music.stop()
        self.next_scene = None
        self.go_next = True


class AnimatedSprite(pg.sprite.Sprite, PositionPropertyMixin):
    images = []
    anim_cycle = 12

    def __init__(self, images=None, containers=None):
        super(AnimatedSprite, self).__init__(containers)

        if images:
            self.images = images

        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.frame = 0

    def update_image(self):
        self.frame = self.frame + 1
        frame_number = self.frame // self.anim_cycle % len(self.images)
        self.image = self.images[frame_number]

    def update(self):
        self.update_image()


# ------------------------------------------- SPRITES -------------------------------------------
class Player(AnimatedSprite):
    speed = 15
    anim_cycle = 12
    images = []
    smile_images = []
    normal_images = []

    def __init__(self, images=None, containers=None):
        super(Player, self).__init__(images, containers)
        self._move_up = False
        self._move_down = False
        self.normal_images = self.images
        self._smile = False
        self._smile_counter = 0

    def gun_position(self):
        x, y = self.position
        return x + 140, y + 100

    def move_up(self):
        self._move_up = True
        self._move_down = False

    def move_down(self):
        self._move_down = True
        self._move_up = False

    def stop(self):
        self._move_up = False
        self._move_down = False

    def smile(self):
        self.images = self.smile_images
        self._smile = True

    def unsmile(self):
        self.images = self.normal_images
        self._smile_counter = 0
        self._smile = False

    def update(self):
        if self._smile:
            if self._smile_counter >= 15:
                self.unsmile()
            self._smile_counter += 1

        self.update_image()

        if self._move_down:
            x, y = self.position
            y += self.speed
            self.position = x, y

        elif self._move_up:
            x, y = self.position
            y -= self.speed
            self.position = x, y


class Bug(AnimatedSprite):
    speed = 1

    anim_cycle = 8
    images = []

    def __init__(self, images=None, containers=None):
        super(Bug, self).__init__(images, containers)

        if images:
            self.images = images

        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.frame = 0

        self.angle = 0
        self.vertical_speed = 1

    def update_position(self):
        x, y = self.position
        x -= self.speed
        w, h = SCREEN_SIZE
        if y >= h - 100 and self.vertical_speed > 0:
            self.vertical_speed *= -1

        self.vertical_speed += 0.5
        y = int(self.vertical_speed + y)
        self.position = x, y

    def get_next_angle(self):
        self.angle += 1
        return self.angle % 360

    def update(self):
        self.update_image()
        self.update_position()
        self.image = pg.transform.rotate(self.image, self.get_next_angle())
        # self.rect = pg.Rect(*self.position, *self.image.get_size())


class Poison(AnimatedSprite):
    speed = 15
    anim_cycle = 12
    images = []

    def __init__(self, images=None, containers=None):
        super(Poison, self).__init__(images, containers)

        if images:
            self.images = images

        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.frame = 0
        self.angle = 0
        self.angle_speed = randint(1, 3)

    def get_next_angle(self):
        self.angle += self.angle_speed
        return self.angle % 360

    def move(self):
        x, y = self.position
        x += self.speed
        self.position = x, y

    def update(self):
        self.update_image()
        self.image = pg.transform.rotate(self.image, self.get_next_angle())
        self.move()


class Gore(AnimatedSprite):
    speed = 15
    anim_cycle = 12
    images = []

    def __init__(self, images=None, containers=None):
        pg.sprite.Sprite.__init__(self, containers)

        if images:
            self.images = images

        self.image = pg.transform.rotate(self.images[0], randint(0, 180))
        self.images = [self.image, ]
        self.rect = self.image.get_rect()
        self.frame = 0


class Tile(pg.sprite.Sprite, PositionPropertyMixin):
    def __init__(self, image=None, containers=None):
        pg.sprite.Sprite.__init__(self, containers)
        self.image = image
        self.rect = self.image.get_rect()
        self._hidden = False
        self._hidden_counter = 0

    def hide(self):
        """
        "спрятать" плитку на полсекунды, т.е. 30 кадров
        """
        self._hidden = True
        self._hidden_counter = 0
        x, y = self.position
        y += 1000
        self.position = x, y

    def unhide(self):
        self._hidden = False
        self._hidden_counter = 0
        x, y = self.position
        y -= 1000
        self.position = x, y

    def update(self):
        if self._hidden:
            self._hidden_counter += 1

        if self._hidden_counter >= 30:
            self.unhide()


class TextSprite(pg.sprite.Sprite, SizePropertyMixin):
    def __init__(
            self, text, pos,
            font_color=BLACK, bg_color=None, font_name=DEFAULT_FONT, font_size=12,
            image=None, containers=None, **_,
    ):
        pg.sprite.Sprite.__init__(self, containers)

        self.text = text
        self.position = pos
        self.font_color = font_color
        self.font_name = font_name
        self.font_size = font_size
        self.bg_color = bg_color

        self.font = pg_font.Font(font_name, font_size)
        self.image = image

        self.rect = None
        self.centery = _.get("centery")
        self.centerx = _.get("centerx")
        self.render()

    def render(self):
        self.image, self.rect = self.font.render(self.text, fgcolor=self.font_color, bgcolor=self.bg_color)

        if self.centerx:
            self.rect.centerx = self.centerx
        if self.centery:
            self.rect.centery = self.centery


class BugTitleSprite(AnimatedSprite):
    anim_cycle = 4


class BonaBug(AnimatedSprite):
    direction = 1
    speed = 3

    def update(self):
        self.update_image()
        x, y = self.position
        if y >= SCREEN_SIZE[1] - 320 or y <= 20:
            self.direction *= -1
        y += self.direction * self.speed
        self.position = x, y


# ------------------------------------------- WIDGETS -------------------------------------------
class Text:
    def __init__(
            self, text, pos,
            font_color=BLACK, bg_color=None, font_name=DEFAULT_FONT, font_size=12,
            **_
    ):
        self.text = text
        self.position = pos
        self.font_color = font_color
        self.font_name = font_name
        self.font_size = font_size
        self.bg_color = bg_color

        self.font = pg_font.Font(font_name, font_size)
        self.image = None
        self.rect = None

        self.centery = _.get("centery")
        self.centerx = _.get("centerx")

    def render(self):
        self.image, self.rect = self.font.render(self.text, fgcolor=self.font_color, bgcolor=self.bg_color)
        # self.rect = self.image.get_rect()
        if self.centerx:
            self.rect.centerx = self.centerx
        if self.centery:
            self.rect.centery = self.centery

        # if type(self.position) is not str:
        #     self.rect.topleft = self.position

    def draw(self, screen):
        if not (self.image and self.rect):
            self.render()

        if self.position == "center":
            scr_w, scr_h = screen.get_size()
            self.rect.centerx = scr_w // 2
            self.rect.centery = scr_h // 2

        if self.centery:
            self.rect.centery = self.centery

        if self.centerx:
            self.rect.centerx = self.centerx

        screen.blit(self.image, self.rect)
        return self.rect


class HighlightedText(Text):
    def __init__(self, text, pos, hl_color=None, **kwargs):
        super(HighlightedText, self).__init__(text, pos, **kwargs)
        self.hl_color = hl_color
        self.highlight = False

    def render(self):
        if not self.hl_color or not self.highlight:
            super(HighlightedText, self).render()
        else:
            self.image, self.rect = self.font.render(self.text, fgcolor=self.hl_color, bgcolor=self.bg_color)


class Bar(PositionPropertyMixin, SizePropertyMixin):
    def __init__(
            self, left, top, width, height,
            rect_colour=BLACK, bg_colour=WHITE, progress_colour=RED
    ):
        self.rect = pg.Rect(left, top, width, height)
        self.percentage = 0
        self.rect_colour = rect_colour
        self.bg_colour = bg_colour
        self.progress_colour = progress_colour

    def draw(self, screen):
        """
        рисуем 3 прямоугольника,
        1. рамка, self.rect
        2. прогресс шириной N% от width, смещение 1 пиксель
        3. фон шириной (100 - N)% от width, смещение x + (width - фон_width)
        """
        width, height = self.size
        x, y = self.position

        # pg.Rect(left, top, width, height)
        # прогресс
        progress_bar = pg.Rect(x + 2, y + 2, int(width * self.percentage), height - 3)

        # фон
        bg_bar = pg.Rect(
            x + 2 + int(width * self.percentage),
            y + 2,
            int(width * (1 - self.percentage)) - 2,
            height - 3
        )

        pg.draw.rect(screen, self.progress_colour, progress_bar)
        pg.draw.rect(screen, self.bg_colour, bg_bar)
        pg.draw.rect(screen, self.rect_colour, self.rect, width=2)
        return pg.Rect(x, y, width + 1, height + 1)
        # pg.display.update(self.rect)


# ------------------------------------------- SCENES -------------------------------------------
class ExitScene(Scene):
    """
    сцена выхода из игры
    """

    BACKGROUND = BLACK
    next_scene = None

    def __init__(self, *args, **kwargs):
        super(ExitScene, self).__init__(*args, **kwargs)
        self.go_next = True


class WinScene(Scene):
    """
    сцена выхода из игры
    """

    BACKGROUND = BLACK
    next_scene = "MainMenu"
    _text = "you win. for now"

    def __init__(self, *args, **kwargs):
        super(WinScene, self).__init__(*args, **kwargs)
        # self.sound_fx = load_sound("audio/fx/win_fx.wav")
        self.music = load_sound("audio/chiptune_1_1.wav")
        self.music.play()
        self.end_point = 60 * 5
        self.text_sprite = self.create_text_sprite()
        rect = self.screen.get_rect()
        self.text_sprite.centerx = rect.centerx
        self.text_sprite.centery = rect.centery

    def create_text_sprite(self):
        rect = self.screen.get_rect()
        centerx, centery = rect.centerx, rect.centery
        return TextSprite(
            self._text, None,
            font_size=36, containers=self.container,
            centerx=centerx, centery=centery,
            bg_color=BLACK, font_color=WHITE
        )

    def update(self):
        self.container.update()
        self.frame_counter += 1
        if self.frame_counter >= self.end_point:
            self.music.stop()
            self.go_next = True


class GameOverScene(Scene):
    next_scene = "MainMenu"
    BACKGROUND = WHITE
    _text = "K /\\ O n bI  W I N"

    def __init__(self, *args, **kwargs):
        super(GameOverScene, self).__init__(*args, **kwargs)

        self.font_size = 10
        self.text = Text(self._text, pos="center", font_size=self.font_size)

        self.start_fx = load_sound("audio/fx/start.wav")
        self.start_fx.play()
        self.end_point = 60 * 6
        self.step = 1
        self.counter = 0

    def get_next_color(self):
        _r = min(255, 20 + int(255 * (self.counter / self.end_point)))
        _g = int(255 * (self.counter / self.end_point))
        _b = int(255 * (self.counter / self.end_point))
        return _r, _g, _b

    def render(self):
        self.screen.fill(self.get_next_color())

        self.counter += 1
        if self.counter % self.step == 0 and self.counter < self.end_point - 60 * 4:
            self.font_size += 0.5

        self.text = Text(self._text, pos="center", font_size=self.font_size, bg_color=self.get_next_color())
        self.text.draw(self.screen)

        if self.counter >= self.end_point:
            self.go_next = True

        pg.display.update(self.screen.get_rect())


class HallwayScene(Scene):
    """
    битва в коридоре
    """

    BACKGROUND = choice(["hall_2.png", "hall_1.gif"])
    BUG_SPEED = 2
    SCORE = 0
    max_time = C_1MIN

    def __init__(self, *args, **kwargs):
        super(HallwayScene, self).__init__(*args, **kwargs)

        self.music = load_sound("audio/zhizha2.wav")
        self.music.play(-1)

        self.bugs = pg.sprite.Group()
        self.shots = pg.sprite.Group()

        self.background = load_image(self.BACKGROUND)
        self.player = Player(images=[load_image(_) for _ in [
            "ivan/ivan_cf_1.gif",
            "ivan/ivan_cf_2.gif",
            "ivan/ivan_cf_3.gif",
            "ivan/ivan_cf_4.gif",
        ]], containers=self.container)
        self.player.position = 50, 100
        self.player.smile_images = [load_image(_) for _ in [
            "ivan/ivan_cf_smile_1.gif",
            "ivan/ivan_cf_smile_2.gif",
            "ivan/ivan_cf_smile_3.gif",
            "ivan/ivan_cf_smile_4.gif",
        ]]

        self.bug_images = [load_image(_) for _ in [
            "klop.gif",
        ]]

        self.poison_image_sets = [
            [load_image(_) for _ in im_set]
            for im_set in POISON_IMAGE_SETS
        ]

        self.frame_counter = 0

        self.boss = BonaBug(
            images=[load_image(choice(["bonaklop.gif", "bonapon.gif"])), ],
            containers=self.container
        )
        self.boss.position = 580, 30

        self.progress_bar = Bar(100, 50, 200, 40)
        self.score_sprite = self.create_score_sprite()

    def create_score_sprite(self):
        rect = self.screen.get_rect()
        centerx, centery = rect.centerx, rect.centery
        return TextSprite(
            f"SCORE: {self.SCORE}", None,
            font_color=WHITE,
            font_size=36, containers=self.container,
            centerx=centerx, centery=centery
        )

    @property
    def poison_images(self):
        return choice(self.poison_image_sets)

    def random_position(self):
        w, h = self.screen_size
        return randint(w, w + 60), randint(0, h // 4)

    def create_bug(self):
        initial_position = self.random_position()
        bug = Bug(images=self.bug_images, containers=(self.container, self.bugs))
        bug.speed = self.BUG_SPEED
        bug.position = initial_position

    def shoot(self):
        initial_position = self.player.gun_position()
        poison = Poison(images=self.poison_images, containers=(self.container, self.shots))
        poison.speed = 10
        poison.position = initial_position

    def process_input(self, events, pressed_keys):
        for event in events:
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP:
                    self.player.move_up()

                elif event.key == pg.K_DOWN:
                    self.player.move_down()

                elif event.key == pg.K_SPACE:
                    self.shoot()

            if event.type == pg.KEYUP:
                self.player.stop()

    @property
    def difficulty(self):
        return min(C_4MIN / (self.frame_counter + 1), 20)

    def win(self):
        self.next_scene = WinScene
        self.music.stop()
        self.go_next = True

    def render(self):
        pg.display.update(self.container.draw(self.screen))
        pg.display.update(self.progress_bar.draw(self.screen))

    def update_score(self):
        self.score_sprite.kill()
        self.score_sprite = self.create_score_sprite()

    def update(self):
        self.frame_counter += 1
        if self.frame_counter % int(self.difficulty) == 0:
            self.create_bug()

        if self.frame_counter > self.max_time:
            self.win()

        for _ in pg.sprite.spritecollide(self.player, self.bugs, True):
            self.player.kill()
            self.next_scene = GameOverScene
            self.music.stop()
            self.go_next = True

        for _ in pg.sprite.groupcollide(self.shots, self.bugs, True, True).keys():
            self.SCORE += 1
            self.update_score()
            self.player.smile()

        self.progress_bar.percentage = min(.99, self.frame_counter / self.max_time)
        self.container.update()


class BedroomScene(Scene):
    """
    битва в постели
    """
    BACKGROUND = "bedroom.gif"
    music: pg.mixer.Sound = "audio/chiptune2.wav"
    BUG_SPEED = 2
    max_time = C_2MIN + 20 * 60
    SCORE = 0

    def __init__(self, *args, **kwargs):
        super(BedroomScene, self).__init__(*args, **kwargs)

        self.bugs = pg.sprite.Group()

        # попытка сделать плитку
        # self.tiles = pg.sprite.Group()
        # self.tile_images = [
        #     load_image(f"blanket/blanket_{_}.gif") for _ in range(1, 1 + 8*6)
        # ]
        # counter = 0
        # for x in range(8):
        #     for y in range(6):
        #         self.create_tile(self.tile_images[counter], 100*x, 100*y)
        #         counter += 1

        self.bug_images = [load_image(_) for _ in [
            "klop.gif",
        ]]

        self.gore_images = [load_image(_) for _ in [
            "gore/001.gif",
            "gore/002.gif",
            "gore/003.gif",
            "gore/004.gif",
        ]]

        self.progress_bar = Bar(100, 50, 200, 40)
        self.left_rect = pg.Rect(-40, -40, 20, 600)
        self.score_sprite = self.create_score_sprite()

    def create_score_sprite(self):
        rect = self.screen.get_rect()
        centerx, centery = rect.centerx, rect.centery
        return TextSprite(
            f"SCORE: {self.SCORE}", None,
            font_size=36, containers=self.container,
            centerx=centerx, centery=centery
        )

    def random_position(self):
        w, h = self.screen_size
        return randint(w, w + 150), randint(0, h // 4)

    # создать плитку
    # def create_tile(self, image, x, y):
    #     tile = Tile(image=image, containers=(self.tiles, self.container))
    #     tile.position = x, y

    def process_input(self, events, pressed_keys):
        for event in events:
            if event.type == pg.MOUSEBUTTONUP:
                pos = pg.mouse.get_pos()
                # скрыть плитку по клику
                # [s.hide() for s in self.tiles if s.rect.collidepoint(pos)]
                for bug in self.bugs:
                    if bug.rect.collidepoint(pos):
                        self.create_gore(bug.rect)
                        bug.kill()

    def create_gore(self, rect):
        self.update_score()
        Gore(images=[choice(self.gore_images), ], containers=self.container).position = rect.x, rect.y

    def create_bug(self):
        initial_position = self.random_position()
        bug = Bug(images=self.bug_images, containers=(self.bugs, self.container))
        bug.speed = self.BUG_SPEED
        bug.position = initial_position

    def update_score(self):
        self.SCORE += 1
        self.score_sprite.kill()
        self.score_sprite = self.create_score_sprite()

    def game_over(self):
        self.next_scene = GameOverScene
        self.music.stop()
        self.go_next = True

    def win(self):
        self.next_scene = WinScene
        self.music.stop()
        self.go_next = True

    def render(self):
        pg.display.update(self.container.draw(self.screen))
        pg.display.update(self.progress_bar.draw(self.screen))

    @property
    def difficulty(self):
        return min(60 * 4, max(int(C_20MIN / (self.frame_counter + 1)), 60))

    def update(self):
        self.frame_counter += 1

        if self.frame_counter % self.difficulty == 0:
            self.create_bug()

        if self.frame_counter > self.max_time:
            self.win()

        [self.game_over() for bug in self.bugs if self.left_rect.colliderect(bug.rect)]

        self.progress_bar.percentage = min(.99, self.frame_counter / self.max_time)
        self.container.update()


class MainMenu(Scene):
    NEXT_SCENES = {
        "HALLWAY": HallwayScene,
        "BEDROOM": BedroomScene,
        "EXIT": ExitScene
    }
    # BACKGROUND = BLACK
    BACKGROUND = "backgg.png"

    def __init__(self, *args, **kwargs):
        super(MainMenu, self).__init__(*args, **kwargs)

        self.list_selection = ListSelection(
            "HALLWAY", "BEDROOM",
            # "KITCHEN", "TOILET",
            "EXIT",
            centerx=self.screen.get_width() // 2
        )

        self.text_list = self.get_text_list()
        self.plop_sound = load_sound("audio/fx/plop.wav")
        self.music = load_sound("audio/someething_1.wav")
        self.music.play(-1)

        self.title_sprite = BugTitleSprite(
            images=[load_image(_) for _ in KK_IMAGES],
            containers=self.container
        )
        self.title_sprite.position = 50, 20

    def get_text_list(self):
        _list = []
        y_size = self.list_selection.vertical_size()
        height = self.screen.get_height()
        y_add = (height - y_size) // 2
        for number, text in enumerate(self.list_selection.CHOICES):
            x, y = self.list_selection.position(number)
            _list.append(
                HighlightedText(
                    text,
                    pos=None, centerx=x, centery=y + y_add,
                    font_size=36, hl_color=RED, font_color=WHITE,
                )
            )

        return _list

    def process_input(self, events, pressed_keys):
        for event in events:
            if event.type == pg.KEYUP:
                if event.key == pg.K_UP:
                    self.list_selection.move_up()
                elif event.key == pg.K_DOWN:
                    self.list_selection.move_down()
                elif event.key == pg.K_RETURN:
                    self.next_scene = self.NEXT_SCENES[self.list_selection.selected()]
                    self.go_next = True
                    self.music.stop()

                if event.key == pg.K_UP or event.key == pg.K_DOWN:
                    self.plop_sound.play()

    def render(self):
        for _, text in enumerate(self.text_list):
            if self.list_selection.caret_position == _:
                text.highlight = True
            else:
                text.highlight = False
            text.render()
            text.draw(screen=self.screen)
        pg.display.update(self.container.draw(self.screen))
        pg.display.update(self.screen.get_rect())


class StartScreen(Scene):
    next_scene = MainMenu
    # next_scene = HallwayScene
    BACKGROUND = WHITE

    def __init__(self, *args, **kwargs):
        super(StartScreen, self).__init__(*args, **kwargs)
        # self.text = Text("KLOPKILLER 2020", pos="center", font_size=48)

        self.font_size = 10
        self.text = Text("KLOPKILLER 2020", pos="center", font_size=self.font_size)

        self.start_fx = load_sound("audio/fx/start.wav")
        self.start_fx.play()
        self.end_point = 60 * 6
        self.step = 1
        self.counter = 0

    def get_next_colour(self):
        _r = min(255, 20 + int(255 * (self.counter / self.end_point)))
        _g = int(255 * (self.counter / self.end_point))
        _b = int(255 * (self.counter / self.end_point))
        return _r, _g, _b

    def render(self):
        self.screen.fill(self.get_next_colour())

        self.counter += 1
        if self.counter % self.step == 0 and self.counter < self.end_point - 60 * 4:
            self.font_size += 0.5

        self.text = Text("KLOPKILLER 2020", pos="center", font_size=self.font_size, bg_color=self.get_next_colour())
        self.text.draw(self.screen)

        if self.counter >= self.end_point:
            self.go_next = True

        pg.display.update(self.screen.get_rect())


class Game:
    SCENES = {
        "start_screen": StartScreen,
        "main_menu": MainMenu,
        "hallway": HallwayScene,
        "bedroom": BedroomScene,
    }

    TITLE = "KLOPKILLER"
    ICON = "icon.png"

    def __init__(self, target_fps, screen_size):
        # инициализация подсистем
        pg.init()
        pg_font.init()
        pg.mixer.pre_init(48000, 32, 2, 1024)

        # Дисплей
        best_depth = pg.display.mode_ok(screen_size, 0, 32)
        self.screen = pg.display.set_mode(screen_size, 0, best_depth)
        self.screen_size = screen_size

        global SCREEN_SIZE
        SCREEN_SIZE = screen_size

        self.target_fps = target_fps
        pg.display.flip()

        # декорации окна
        icon = load_image(self.ICON)
        pg.display.set_icon(icon)
        pg.display.set_caption(self.TITLE)

        # Время
        self.clock = pg.time.Clock()

        # сцена
        self.active_scene = self.get_starting_scene()

    def get_starting_scene(self):
        return self.SCENES["start_screen"](self.screen)

    @property
    def is_running(self):
        return self.active_scene is not None

    @staticmethod
    def is_quit_event(event, pressed_keys):
        """
        отслеживает событие выхода из игры
        """
        x_out = event.type == pg.QUIT
        ctrl = pressed_keys[pg.K_LCTRL] or pressed_keys[pg.K_RCTRL]
        q = pressed_keys[pg.K_q]

        return x_out or (ctrl and q)

    def update_scene(self):
        """
        выполняет переход к следующей сцене при окончании текущей
        """
        if self.active_scene.go_next:
            next_scene = self.active_scene.next_scene
            if next_scene is None:
                self.active_scene = None
                return
            elif type(next_scene) is str:
                next_scene = eval(next_scene)
            self.active_scene = next_scene(self.screen)

    def run(self):
        while self.is_running:

            pressed_keys = pg.key.get_pressed()
            filtered_events = []

            for event in pg.event.get():
                if self.is_quit_event(event, pressed_keys):
                    self.active_scene.terminate()
                elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    self.active_scene.terminate()
                    if type(self.active_scene) is self.SCENES["main_menu"]:
                        self.active_scene.terminate()
                    else:
                        self.active_scene.next_scene = self.SCENES["main_menu"]
                        self.active_scene.go_next = True
                else:
                    filtered_events.append(event)

            self.active_scene.process_input(filtered_events, pressed_keys)
            self.active_scene.clear()
            self.active_scene.update()
            self.active_scene.render()
            self.update_scene()

            self.clock.tick(self.target_fps)

        pg.quit()
