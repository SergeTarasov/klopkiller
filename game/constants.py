poison_prefix = "poison"
POISON_IMAGE_SETS = [
    [f"{poison_prefix}/p_{set_number}_{im_number}.gif" for im_number in range(1, 9)]
    for set_number in range(1, 5)
]

KK_IMAGES = [
    "kkk/{:02d}.gif".format(1 + _) for _ in range(20)
]
