class ListSelection:
    # Нумерация сверху вниз
    caret_position = 0
    margin = 20
    text_height = 40

    CHOICES = ()

    def __init__(self, *args, centerx=None):

        self.centerx = centerx
        self.CHOICES = args

    def position(self, number):
        return self.centerx, (self.text_height + self.margin) * number

    def move_up(self):
        self.caret_position = (self.caret_position - 1) % len(self.CHOICES)

    def move_down(self):
        self.caret_position = (self.caret_position + 1) % len(self.CHOICES)

    def vertical_size(self):
        return len(self.CHOICES) * (self.text_height + self.margin) - self.margin

    def selected(self):
        return self.CHOICES[self.caret_position]


class PositionPropertyMixin:
    @property
    def position(self):
        return self.rect.x, self.rect.y

    @position.setter
    def position(self, value):
        x, y = value
        self.rect.x, self.rect.y = x, y


class SizePropertyMixin:
    @property
    def size(self):
        return self.rect.width, self.rect.height

    @size.setter
    def size(self, value):
        x, y = value
        self.rect.width, self.rect.height = x, y
