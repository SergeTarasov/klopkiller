from game.game import Game


def main():
    target_fps = 60
    # всё сцены подготовлены как 800x600
    screen_size = 800, 600
    game = Game(target_fps, screen_size)
    game.run()


if __name__ == '__main__':
    main()
